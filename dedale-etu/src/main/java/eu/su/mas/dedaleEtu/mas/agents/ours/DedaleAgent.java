package eu.su.mas.dedaleEtu.mas.agents.ours;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.domain.DFService;
import jade.domain.FIPAException;

public class DedaleAgent extends AbstractDedaleAgent {

	private static final long serialVersionUID = 1L;
	private MapRepresentation myMap;
	private String service;
	private HashMap<String, ArrayList<String>> agentsOnMap = new HashMap<>();
	
	/**
	 * Dernière position connue des autres agents sur la carte, structure : < Role de l'agent (explorateur,collecteur,etc.) : < Nom de l'agent , Position de l'agent >>
	 */
	private HashMap<String, HashMap<String, String>> posOtherAgentByRole = new HashMap<String, HashMap<String, String>>();
	
	private HashMap<String, HashMap<String, String>> treasuresKnowledge = new HashMap<String, HashMap<String, String>>();
	
	private Set<String> openTreasures = new HashSet<>();
	
	private Set<String> closedTreasures = new HashSet<>();

	public MapRepresentation getMap() {
		return myMap;
	}

	public void setMap(MapRepresentation myMap) {
		this.myMap = myMap;
	}
	
	public ArrayList<String> getExplorersOnMap() {
		return this.agentsOnMap.get("explorers");
	}
	
	public ArrayList<String> getCollectorsOnMap() {
		return this.agentsOnMap.get("collectors");
	}
	
	public ArrayList<String> getTankersOnMap() {
		return this.agentsOnMap.get("tankers");
	}
	
	public ArrayList<String> getAllAgentsOnMap() {
		ArrayList<String> agentsOnMap = new ArrayList<String>();
		if (this.getExplorersOnMap()!= null) agentsOnMap.addAll(this.getExplorersOnMap());
		if (this.getCollectorsOnMap()!= null) agentsOnMap.addAll(this.getCollectorsOnMap());
		if (this.getTankersOnMap()!= null) agentsOnMap.addAll(this.getTankersOnMap());
		return agentsOnMap;
	}
	
	public void setAgentsOnMapLocalNames(HashMap<String, ArrayList<String>> l) {
		this.agentsOnMap = l;
	}
	
	public String getService() {
		return service;
	}
	
	public void setService(String s) {
		this.service = s;
	}
	
	public HashMap<String, HashMap<String, String>> getPosOtherAgentByRole() {
		return posOtherAgentByRole;
	}

	public void setPosOtherAgentByRole(HashMap<String, HashMap<String, String>> posOtherAgentByRole) {
		this.posOtherAgentByRole = posOtherAgentByRole;
	}

	public HashMap<String, HashMap<String, String>> getTreasuresKnowledge() {
		return this.treasuresKnowledge;
	}
	
	public void setTreasuresKnowledge(HashMap<String, HashMap<String, String>> treasuresKnowledge) {
		if (treasuresKnowledge != null) {
			this.treasuresKnowledge = treasuresKnowledge;
			for (String treasureId : this.treasuresKnowledge.keySet()) {
				if (this.treasuresKnowledge.get(treasureId).get("isOpen").equals("1")) {
					this.openTreasures.add(treasureId);
				} else {
					this.closedTreasures.add(treasureId);
				}
			}
		}
	}
	
	public Set<String> getOpenTreasures() {
		return this.openTreasures;
	}
	
	public Set<String> getClosedTreasures() {
		return this.closedTreasures;
	}
	
	@Override
	public void takeDown() {
		try	{
			DFService.deregister(this);
		}
		catch(FIPAException fe) {
			fe.printStackTrace();
		}
		System.out.println(this.getLocalName()+" terminating");
		super.takeDown();
	}

}
