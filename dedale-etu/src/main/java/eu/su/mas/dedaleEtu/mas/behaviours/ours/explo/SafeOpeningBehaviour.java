package eu.su.mas.dedaleEtu.mas.behaviours.ours.explo;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.ours.DedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.OneShotBehaviour;


public class SafeOpeningBehaviour extends OneShotBehaviour{
	
	private static final long serialVersionUID = 9088209402507795289L;
	
	
	/**
	 * Noeuds contenant un coffre, ce coffre ayant déjà été ouvert
	 */
	private Set<String> closedNodes = new HashSet<String>();

	private HashMap<String,HashMap<String,String>> treasuresKnowledge = new HashMap<String, HashMap<String, String>>();
	
	/**
	 * Permet de retenir la date de la dernière maj des connaissances pour chaque trésor
	 */
	private Date d = new Date();

	public SafeOpeningBehaviour (final AbstractDedaleAgent myagent) {
		super(myagent);
	}

	@Override
	public void action() {
		//System.out.println(this.myAgent.getLocalName() + " SAFE OPENING BEHAVIOUR ");
		
		//Example to retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		//System.out.println(this.myAgent.getLocalName()+" -- myCurrentPosition is: "+myPosition);
		if (myPosition!=null){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			//System.out.println(this.myAgent.getLocalName()+" -- list of observables: "+lobs);
			
			//list of observations associated to the currentPosition
			List<Couple<Observation,Integer>> lObservations= lobs.get(0).getRight();
			//System.out.println(this.myAgent.getLocalName()+" -- " + lObservations);

			Boolean b=false;
			//example related to the use of the backpack for the treasure hunt
			for (int i = 0; i < lobs.size(); i++) {
				String nodeId = lobs.get(i).getLeft();
				List<Couple<Observation,Integer>> obs = lobs.get(i).getRight();
				HashMap<String,String> characteristics = new HashMap<String,String>();
				if (treasuresKnowledge.get(nodeId) != null) {
					characteristics = treasuresKnowledge.get(nodeId);
				}
				for(Couple<Observation,Integer> o:obs){
					switch (o.getLeft()) {
					case DIAMOND:case GOLD:
						//System.out.println(this.myAgent.getLocalName()+" - Value of the treasure on the current position: "+o.getLeft() +": "+ o.getRight());
						boolean success = false;
						if (!closedNodes.contains(nodeId)) success = ((AbstractDedaleAgent) this.myAgent).openLock(o.getLeft());
						//System.out.println(this.myAgent.getLocalName()+" - I try to open the safe: "+success);
						if (success) closedNodes.add(nodeId); // si on a ouvert un coffre on le considere comme fermé (on n'a plus a aller dessus
						characteristics.put("type", o.getLeft().toString());
						characteristics.put("value", o.getRight().toString());
						b=true;
						break;
					case LOCKSTATUS:
						characteristics.put("isOpen", o.getRight().toString());
						if (o.getRight() == 1) {
							closedNodes.add(nodeId);
						}
						break;
					case STRENGH:
						characteristics.put("strengthRequired", o.getRight().toString());
						break;
					case LOCKPICKING:
						characteristics.put("lockpickingRequired", o.getRight().toString());
						break;
					default:
						break;
					}
				}
				if (!characteristics.isEmpty()) {
					characteristics.put("lastMaj",Long.toString(d.getTime()));
					treasuresKnowledge.put(nodeId, characteristics);
					((DedaleAgent)this.myAgent).setTreasuresKnowledge(treasuresKnowledge);;
				}
			}
			//If the agent picked (part of) the treasure
			if (b){
				List<Couple<String,List<Couple<Observation,Integer>>>> lobs2=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
				//System.out.println(this.myAgent.getLocalName()+" - State of the observations after trying to pick something "+lobs2);
			}

			//Random move from the current position
			Random r= new Random();
			int moveId=1+r.nextInt(lobs.size()-1);//removing the current position from the list of target, not necessary as to stay is an action but allow quicker random move

			//System.out.println(this.myAgent.getLocalName()+" - treasures Knowledge " + treasuresKnowledge);
			
			//The move action (if any) should be the last action of your behaviour
			((AbstractDedaleAgent)this.myAgent).moveTo(lobs.get(moveId).getLeft());
		}

	}

//	@Override
//	public boolean done() {
//		return false;
//	}
	
	@Override
	public int onEnd(){
		System.out.println("FIN RECEIVE TK BEHAVIOUR");
		return 7 ;
	}

}