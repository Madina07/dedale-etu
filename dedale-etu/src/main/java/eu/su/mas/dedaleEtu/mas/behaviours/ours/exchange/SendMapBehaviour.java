package eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange;

import java.io.IOException;
import java.util.ArrayList;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.ours.DedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class SendMapBehaviour extends OneShotBehaviour {

	
	private static final long serialVersionUID = 1L;
	private Couple<ArrayList<Couple<String,MapAttribute>>,ArrayList<Couple<String,String>>> previousSentMap = new Couple(new ArrayList<>(),new ArrayList<>());
	private Couple<ArrayList<Couple<String,MapAttribute>>,ArrayList<Couple<String,String>>> mapToSend = new Couple(new ArrayList<>(),new ArrayList<>());
	private int sameMapCounter = 0;
	private int noRequest = 0;
	private boolean finished = false;
	
	public SendMapBehaviour(final Agent myAgent) {
		super(myAgent);
	}

	@Override
	public void action() {
		System.out.println(this.myAgent.getLocalName() + " SEND MAP BEHAVIOUR");
//		final MessageTemplate requestTemplate = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
//		final ACLMessage request = this.myAgent.receive(requestTemplate);
//		if (request != null) {
//			if (request.getProtocol() == "Send map please") {
				String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
				ACLMessage map = new ACLMessage(ACLMessage.INFORM);
				map.setSender(this.myAgent.getAID());
				map.setProtocol("Send map");
				//if (myPosition!="" && !((DedaleAgent)this.myAgent).getAllAgentsOnMap().isEmpty()){
				if (myPosition!=""){
					//System.out.println(this.myAgent.getLocalName()+ " is trying to send his map");
					try {
						if(((DedaleAgent)this.myAgent).getMap()!=null) {
							mapToSend = ((DedaleAgent) this.myAgent).getMap().getSerializableMap();
							map.setContentObject(mapToSend);
							//System.out.println(this.myAgent.getLocalName()+ " edges to send : " + edgesToSend);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					//System.out.println(this.myAgent.getLocalName()+": Autres agents presents sur la carte:"+((DedaleAgent)this.myAgent).getAgentsOnMapLocalNames());
					
					for (String aln : ((DedaleAgent) this.myAgent).getAllAgentsOnMap())
						map.addReceiver(new AID(aln,AID.ISLOCALNAME));
					
					//Mandatory to use this method (it takes into account the environment to decide if someone is reachable or not)
//					if (previousSentMap.equals(mapToSend)) {
//						sameMapCounter += 1;
//					} else {
//						sameMapCounter = 0;
//					}
//					if (sameMapCounter == 50) {
//						System.out.println(this.myAgent.getLocalName()+ " already sent this same map many times");
//						finished = true;
//					}
					System.out.println(this.myAgent.getLocalName()+ " sends his map : " + mapToSend);
					((AbstractDedaleAgent)this.myAgent).sendMessage(map);
				}
				previousSentMap = mapToSend;
				noRequest = 0;
//			}
//		}
//		else {
//			noRequest += 1;
//			if (noRequest == 10000) {
//				System.out.println(this.myAgent.getLocalName()+ " no one wants a map...");
//				finished = true;
//			}
//		}
	}
	
	@Override
	public int onEnd(){
		System.out.println("FIN SEND MAP BEHAVIOUR");
		return 3;
	}

//	@Override
//	public int done() {
//		if (finished) {
//			System.out.println(this.myAgent.getLocalName()+" : PING AND SEND MAP BEHAVIOUR REMOVED");
//		}
//		return finished;
//	}

}
