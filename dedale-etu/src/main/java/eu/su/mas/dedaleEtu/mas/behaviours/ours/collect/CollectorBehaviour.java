package eu.su.mas.dedaleEtu.mas.behaviours.ours.collect;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.Ping;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.ReceiveMapBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.ReceiveTreasuresKnowledgeBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.SendMapBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.SendTreasuresKnowledgeBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.explo.MapDiscoveryBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.explo.SafeOpeningBehaviour;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.SimpleBehaviour;

public class CollectorBehaviour extends FSMBehaviour {

	public CollectorBehaviour(final AbstractDedaleAgent myagent) {
		super(myagent);
		
		this.registerFirstState(new MapDiscoveryBehaviour(myagent), "Map discovery");
		this.registerState(new Ping(myagent), "Ping");
		this.registerState(new SendMapBehaviour(myagent), "Send map");
		this.registerState(new SendTreasuresKnowledgeBehaviour(myagent), "Send tk");
		this.registerState(new ReceiveMapBehaviour(myagent), "Receive map");
		this.registerState(new ReceiveTreasuresKnowledgeBehaviour(myagent), "Receive tk");
		this.registerLastState(new CollectBehaviour(myagent), "Collect");

		this.registerTransition("Map discovery", "Ping",1);
		this.registerTransition("Ping", "Send map",2);
		this.registerTransition("Send map", "Send tk",3);
		this.registerTransition("Send tk", "Receive map",4);
		this.registerTransition("Receive map", "Receive tk",5);
		this.registerTransition("Receive tk", "Map discovery",6);
		this.registerTransition("Ping","Map discovery",7);
		this.registerTransition("Map discovery", "Collect",8);
		
		
	}
	
	@Override
	public int onEnd(){
		System.out.println("Collector job done");
		myAgent.doDelete();
		return super.onEnd();
	}
}
