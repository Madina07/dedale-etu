package eu.su.mas.dedaleEtu.mas.behaviours.ours.df;

import java.util.ArrayList;
import java.util.HashMap;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.ours.DedaleAgent;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class GetOtherAgentsLocalNamesBehaviour extends WakerBehaviour {
	
	/**
	 * On laisse le temps aux agents de s'enregistrer auprès du DF (1 seconde) puis on récupère leurs noms
	 */
	
	private static final long serialVersionUID = 1L;
	
	public GetOtherAgentsLocalNamesBehaviour(AbstractDedaleAgent myagent){
		super(myagent, 1000);
	}
	
	@Override
	public void onWake() {
		DFAgentDescription dfd_e = new DFAgentDescription();
		ServiceDescription sd_e = new ServiceDescription();
		sd_e.setType("exploring");
		dfd_e.addServices(sd_e);
		DFAgentDescription dfd_c = new DFAgentDescription();
		ServiceDescription sd_c = new ServiceDescription();
		sd_c.setType("collecting");
		dfd_c.addServices(sd_c);
		DFAgentDescription dfd_s = new DFAgentDescription();
		ServiceDescription sd_s = new ServiceDescription();
		sd_s.setType("silo");
		dfd_s.addServices(sd_s);
		try {
			DFAgentDescription[] result_e = DFService.search(super.myAgent, dfd_e);
			DFAgentDescription[] result_c = DFService.search(super.myAgent, dfd_c);
			DFAgentDescription[] result_s = DFService.search(super.myAgent, dfd_s);
			
			HashMap<String,ArrayList<String>> agentsOnMap = new HashMap<String,ArrayList<String>>();
			
			ArrayList<String> explorers = new ArrayList<String>();
			for (DFAgentDescription ad : result_e){
				if(!ad.getName().equals(this.myAgent.getAID())) {
					explorers.add(ad.getName().getLocalName());
				}
			}
			agentsOnMap.put("explorers",explorers);
			
			ArrayList<String> collectors = new ArrayList<String>();
			for (DFAgentDescription ad : result_c){
				if(!ad.getName().equals(this.myAgent.getAID())) {
					collectors.add(ad.getName().getLocalName());
				}
			}
			agentsOnMap.put("collectors",collectors);
			
			ArrayList<String> tankers = new ArrayList<String>();
			for (DFAgentDescription ad : result_s){
				if(!ad.getName().equals(this.myAgent.getAID())) {
					tankers.add(ad.getName().getLocalName());
				}
			}
			agentsOnMap.put("tankers",tankers);
			((DedaleAgent)super.myAgent).setAgentsOnMapLocalNames(agentsOnMap);
			System.out.println(this.myAgent.getLocalName()+": Autres agents presents sur la carte:"+agentsOnMap);
			
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		
	}
}
