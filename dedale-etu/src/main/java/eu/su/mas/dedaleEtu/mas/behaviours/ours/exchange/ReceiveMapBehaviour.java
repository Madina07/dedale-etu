package eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.graphstream.graph.Node;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.ours.DedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class ReceiveMapBehaviour extends OneShotBehaviour {

	private static final long serialVersionUID = 1L;

	private int noOpenNodes = 0;
	private boolean AreThereAnyOpenNodes;
	private boolean finished;
	
	/**
	 * Nodes known but not yet visited
	 */
	private List<String> openNodes = new ArrayList<>();
	/**
	 * Visited nodes
	 */
	private Set<String> closedNodes = new HashSet<String>();
	/**
	 *  Dernières positions connues des autres agents
	 */
	private HashMap<String, HashMap<String, String>> posOtherAgentByRole = new HashMap<String, HashMap<String, String>> ();
	
	private Couple<ArrayList<Couple<String,MapAttribute>>,ArrayList<Couple<String,String>>> receivedMap = new Couple(new ArrayList<>(),new ArrayList<>());

	public ReceiveMapBehaviour(final Agent myagent) {
		super(myagent);
	}


	public void action() {
		System.out.println(this.myAgent.getLocalName() + " RECEIVE MAP BEHAVIOUR");
		
//		this.AreThereAnyOpenNodes = false;
//		ACLMessage msg=new ACLMessage(ACLMessage.REQUEST);
//		msg.setSender(this.myAgent.getAID());
//		msg.setProtocol("Send map please");
//		for (String lc : ((DedaleAgent)this.myAgent).getAllAgentsOnMap()) {
//			msg.addReceiver(new AID(lc,AID.ISLOCALNAME));
//		}
		
		// Tant que sa connaissance de la carte n'est pas complete l'agent demande aux autres qu'ils partagent ses connaissances avec lui
		//System.out.println(this.myAgent.getLocalName() + " wants to receive other agents' maps");
		//Mandatory to use this method (it takes into account the environment to decide if someone is reachable or not)
//		((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
		
		//1) receive the map
		final MessageTemplate msgTemplateMap = MessageTemplate.MatchPerformative(ACLMessage.INFORM);			
		final ACLMessage map = this.myAgent.receive(msgTemplateMap);
		if (map != null) {
			if (map.getProtocol() == "Send map") {
				
				try {
					AID senderAID = map.getSender();
					String senderGroup = null;
					for (String agent : ((DedaleAgent)this.myAgent).getExplorersOnMap()) {
						if (agent.equals(senderAID.getLocalName())) senderGroup = "explorers";
					}
					for (String agent : ((DedaleAgent)this.myAgent).getCollectorsOnMap()) {
						if (agent.equals(senderAID.getLocalName())) senderGroup = "collectors";
					}
					for (String agent : ((DedaleAgent)this.myAgent).getTankersOnMap()) {
						if (agent.equals(senderAID.getLocalName())) senderGroup = "tankers";
					}
					receivedMap = (Couple<ArrayList<Couple<String,MapAttribute>>,ArrayList<Couple<String,String>>>) map.getContentObject();
					System.out.println(this.myAgent.getLocalName()+"<----Map received from agent "+map.getSender().getLocalName()+" ,map = "+receivedMap);
	//				AID senderAID = map.getSender();
	//				senderAID.getLocalName();
					
					ArrayList<Couple<String, MapAttribute>> receivedNodes = new ArrayList<>();
					ArrayList<Couple<String, String>> receivedEdges = new ArrayList<>();
					
					//2) merge maps
					if (!receivedMap.getRight().isEmpty()) {
						//System.out.println(this.myAgent.getLocalName() + " received the followiwng map :"+receivedEdges);
						receivedNodes = receivedMap.getLeft();
						receivedEdges = receivedMap.getRight();
						for (Couple<String,MapAttribute> n : receivedNodes) {
							if (n.getRight().toString() == MapAttribute.open.toString()) { // noeud à explorer chez le sender
								if (!closedNodes.contains(n.getLeft())) {
									((DedaleAgent)this.myAgent).getMap().addNode(n.getLeft(),MapAttribute.open);
								}
							} else if (n.getRight().toString() == MapAttribute.closed.toString()) { // noeud déjà exploré par le sender
								((DedaleAgent)this.myAgent).getMap().addNode(n.getLeft(),MapAttribute.closed);
							} else if (n.getRight().toString() == MapAttribute.agent.toString()) { // noeud agent chez le sender (agents rencontrés)
								((DedaleAgent)this.myAgent).getMap().addNode(n.getLeft(),MapAttribute.closed);
							} else if (n.getRight().toString() == MapAttribute.me.toString()) { // position du sender
								((DedaleAgent)this.myAgent).getMap().addNode(n.getLeft(),MapAttribute.closed);
								HashMap<String, String> previousHM;
								if (posOtherAgentByRole.get(senderGroup) == null) {
									previousHM = new HashMap<String, String>();
								} else {
									previousHM = posOtherAgentByRole.get(senderGroup);
								}
								previousHM.put(senderAID.getLocalName(), n.getLeft());
								HashMap<String, String> newHM = previousHM;
								posOtherAgentByRole.put(senderGroup,newHM);
								((DedaleAgent) this.myAgent).setPosOtherAgentByRole(posOtherAgentByRole);
								//System.out.println(this.myAgent.getLocalName() + " MAJ POSITION DES AUTRES AGENTS " + posOtherAgentByRole);
							}
								
	//						if (!closedNodes.contains(n.getLeft())){
	//							//System.out.println(this.myAgent.getLocalName() + " noeud recu " + n);
	//							if (n.getRight().toString() == MapAttribute.open.toString()) {
	//								((DedaleAgent)this.myAgent).getMap().addNode(n.getLeft(),MapAttribute.open);
	//							} else {
	//								((DedaleAgent)this.myAgent).getMap().addNode(n.getLeft(),MapAttribute.closed);
	//							}
	//							
	//						}
							// en ajoutant que les noeuds en tant que noeuds fermés on ne retient pas la position des autres agents a un instant, réféchir là dessus
						}
						for (Couple<String,String> e : receivedEdges) {
							((DedaleAgent)this.myAgent).getMap().addEdge(e.getLeft(),e.getRight());
						}
					}
					
					for (Node n : ((DedaleAgent)this.myAgent).getMap().getG().getNodeSet()) {
						//System.out.println(this.myAgent.getLocalName() + " : " + n.getId() + " " + n.getAttribute("ui.class"));
						if (n.getAttribute("ui.class") == MapAttribute.open.toString() && !this.closedNodes.contains(n.getId())) {
							if (!this.openNodes.contains(n.getId())) {
								//System.out.println(this.myAgent.getLocalName() + " : adding node " + n.getId() + " to closedNodes");
								this.openNodes.add(n.getId());
							}
						} else {
							this.closedNodes.add(n.getId());
							if (this.openNodes.contains(n.getId())) {
								//System.out.println(this.myAgent.getLocalName() + " : removing node " + n.getId() + " from openNodes");
								this.openNodes.remove(n.getId());
							}
						}
					}
				} catch (UnreadableException e) {
					e.printStackTrace();
				}
			}
			
//			//3) On arrete de recevoir des cartes seulement lorsque l'on connait a priori toute la carte (ie que des noeuds fermes ou agent)
//			for (Node n : ((DedaleAgent)this.myAgent).getMap().getG().getNodeSet()) {
//				if (n.getAttribute("ui.class").toString() == MapAttribute.open.toString()) {
//					this.AreThereAnyOpenNodes = true;
//					break;
//				}
//			}
			
//			if (!AreThereAnyOpenNodes) {
//				noOpenNodes += 1;
//			} else {
//				noOpenNodes = 0;
//			}
//			
//			if (noOpenNodes == 10) {
//				finished = true;
//			}
		}
	}
	
	@Override
	public int onEnd(){
		System.out.println("FIN RECEIVE MAP BEHAVIOUR");
		return 5;
	}

//	public boolean done() {
//		if (finished) {
//			System.out.println(this.myAgent.getLocalName()+" : RECEIVE MESSAGE BEHAVIOUR REMOVED");
//		}
//		return finished;
//	}

}
