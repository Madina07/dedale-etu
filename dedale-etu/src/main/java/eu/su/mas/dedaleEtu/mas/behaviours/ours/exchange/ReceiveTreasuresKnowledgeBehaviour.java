package eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.graphstream.graph.Node;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.ours.DedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class ReceiveTreasuresKnowledgeBehaviour extends OneShotBehaviour {

	private static final long serialVersionUID = 1L;

	private int noOpenNodes = 0;
	private boolean AreThereAnyOpenNodes;
	private boolean finished;
	
	/**
	 * Noeuds contenant un trésor fermé 
	 */
	private List<String> openNodes = new ArrayList<>();
	/**
	 * Noeuds contenant un trésor déjà ouvert
	 */
	private Set<String> closedNodes = new HashSet<String>();
	/**
	 *  Dernières informations connues par l'agent sur les trésors
	 */
	private HashMap<String,HashMap<String,String>> receivedTk = new HashMap<String,HashMap<String,String>>();

	public ReceiveTreasuresKnowledgeBehaviour(final Agent myagent) {
		super(myagent);
	}


	public void action() {
		System.out.println(this.myAgent.getLocalName() + " RECEIVE TK BEHAVIOUR");
		
//		this.AreThereAnyOpenNodes = false;
//		ACLMessage msg=new ACLMessage(ACLMessage.REQUEST);
//		msg.setSender(this.myAgent.getAID());
//		msg.setProtocol("Send treasures knowledge please");
//		for (String lc : ((DedaleAgent)this.myAgent).getAllAgentsOnMap()) {
//			msg.addReceiver(new AID(lc,AID.ISLOCALNAME));
//		}
//		
		// Tant que sa connaissance de la carte n'est pas complete l'agent demande aux autres qu'ils partagent ses connaissances avec lui
		//System.out.println(this.myAgent.getLocalName() + " wants to receive other agents' maps");
		//Mandatory to use this method (it takes into account the environment to decide if someone is reachable or not)
//		((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
		
		//1) receive the map
		final MessageTemplate msgTemplateMap = MessageTemplate.MatchPerformative(ACLMessage.INFORM);			
		final ACLMessage tk = this.myAgent.receive(msgTemplateMap);
		if (tk != null) {
			if (tk.getProtocol() == "Send treasures knowledge") {
				try {
					AID senderAID = tk.getSender();
					String senderGroup = null;
					for (String agent : ((DedaleAgent)this.myAgent).getExplorersOnMap()) {
						if (agent.equals(senderAID.getLocalName())) senderGroup = "explorers";
					}
					for (String agent : ((DedaleAgent)this.myAgent).getCollectorsOnMap()) {
						if (agent.equals(senderAID.getLocalName())) senderGroup = "collectors";
					}
					for (String agent : ((DedaleAgent)this.myAgent).getTankersOnMap()) {
						if (agent.equals(senderAID.getLocalName())) senderGroup = "tankers";
					}
					receivedTk = (HashMap<String,HashMap<String,String>>) tk.getContentObject();
					System.out.println(this.myAgent.getLocalName()+"<----Tk received from agent "+tk.getSender().getLocalName()+" ,tk = "+receivedTk);
					
					//2) merge knowledge (on prend ou non en compte les modifications selon la fraîcheur de l'information (date))
					if (receivedTk!=null) {
						for (String nodeId : receivedTk.keySet()) {
							if (((DedaleAgent)this.myAgent).getTreasuresKnowledge().get(nodeId) != null) {
								if (new Long(receivedTk.get(nodeId).get("lastMaj")) > new Long(((DedaleAgent)this.myAgent).getTreasuresKnowledge().get(nodeId).get("lastMaj"))) {
									//information recue plus récente donc maj
									HashMap<String,String> newInfo = receivedTk.get(nodeId);
									((DedaleAgent)this.myAgent).getTreasuresKnowledge().put(nodeId, newInfo);
									((DedaleAgent)this.myAgent).setTreasuresKnowledge(((DedaleAgent)this.myAgent).getTreasuresKnowledge());
									//System.out.println(this.myAgent.getLocalName()+" - MAJ Treasures knowledge" + ((DedaleAgent)this.myAgent).getTreasuresKnowledge());
									System.out.println(this.myAgent.getLocalName()+" - MAJ Treasures knowledge");
									System.out.println(this.myAgent.getLocalName()+" - new open treasures : " + ((DedaleAgent)this.myAgent).getOpenTreasures());
									System.out.println(this.myAgent.getLocalName()+" - new closed treasures : " + ((DedaleAgent)this.myAgent).getClosedTreasures());
								}
							} else {
								System.out.println(this.myAgent.getLocalName()+" - I don't know node " + nodeId);
								HashMap<String,String> info = receivedTk.get(nodeId);
								((DedaleAgent)this.myAgent).getTreasuresKnowledge().put(nodeId, info);
								((DedaleAgent)this.myAgent).setTreasuresKnowledge(((DedaleAgent)this.myAgent).getTreasuresKnowledge());
								System.out.println(this.myAgent.getLocalName()+" - MAJ Treasures knowledge");
								System.out.println(this.myAgent.getLocalName()+" - new open treasures : " + ((DedaleAgent)this.myAgent).getOpenTreasures());
								System.out.println(this.myAgent.getLocalName()+" - new closed treasures : " + ((DedaleAgent)this.myAgent).getClosedTreasures());
							}
						}
					}
					
				} catch (UnreadableException e) {
					e.printStackTrace();
					
				}
			}
		}
		
//		//3) On arrete de recevoir des cartes seulement lorsque l'on connait a priori toute la carte (ie que des noeuds fermes ou agent)
//		for (Node n : ((DedaleAgent)this.myAgent).getMap().getG().getNodeSet()) {
//			if (n.getAttribute("ui.class").toString() == MapAttribute.open.toString()) {
//				this.AreThereAnyOpenNodes = true;
//				break;
//			}
//		}
		
//		if (!AreThereAnyOpenNodes) {
//			noOpenNodes += 1;
//		} else {
//			noOpenNodes = 0;
//		}
//		
//		if (noOpenNodes == 10) {
//			finished = true;
//		}
	}
	
	@Override
	public int onEnd(){
		System.out.println("FIN RECEIVE TK BEHAVIOUR");
		return 6 ;
	}

//	public boolean done() {
//		if (finished) {
//			System.out.println(this.myAgent.getLocalName()+" : RECEIVE MESSAGE BEHAVIOUR REMOVED");
//		}
//		return finished;
//	}

}
