package eu.su.mas.dedaleEtu.mas.agents.ours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.collect.CollectorBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.df.DFRegisteringBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.df.GetOtherAgentsLocalNamesBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.ReceiveMapBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.ReceiveTreasuresKnowledgeBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.SendMapBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.SendTreasuresKnowledgeBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.explo.ExplorerBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.explo.MapDiscoveryBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;

public class Collector extends DedaleAgent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1784844593772918359L;



	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	protected void setup(){

		super.setup();
		
		this.setService("collecting");

		List<Behaviour> lb=new ArrayList<Behaviour>();
		
		lb.add(new DFRegisteringBehaviour(this)); // s'enregistrer auprès du DF
		lb.add(new GetOtherAgentsLocalNamesBehaviour(this)); // prendre connaissance des autres agents présents sur la carte
		lb.add(new CollectorBehaviour(this));
//		lb.add(new MapDiscoveryBehaviour(this)); // explorer la carte
//		lb.add(new SendMapBehaviour(this));
//		lb.add(new ReceiveMapBehaviour(this));
//		lb.add(new SendTreasuresKnowledgeBehaviour(this));
//		lb.add(new ReceiveTreasuresKnowledgeBehaviour(this));

		addBehaviour(new startMyBehaviours(this,lb));

		System.out.println("the  agent "+this.getLocalName()+ " is started");

	}
	
	public String getService() {
		return "collecting";
	}
}
