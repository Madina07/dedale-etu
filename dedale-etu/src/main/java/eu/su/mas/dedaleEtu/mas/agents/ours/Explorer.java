package eu.su.mas.dedaleEtu.mas.agents.ours;

import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.df.DFRegisteringBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.df.GetOtherAgentsLocalNamesBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.ReceiveMapBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.ReceiveTreasuresKnowledgeBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.SendMapBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.SendTreasuresKnowledgeBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.explo.ExplorerBehaviour;
import jade.core.behaviours.Behaviour;

public class Explorer extends DedaleAgent {
	
	private static final long serialVersionUID = 1L;

	protected void setup(){

		super.setup();
		
		this.setService("exploring");
		
		List<Behaviour> lb=new ArrayList<Behaviour>();
		
		lb.add(new DFRegisteringBehaviour(this)); // s'enregistrer auprès du DF
		lb.add(new GetOtherAgentsLocalNamesBehaviour(this)); // prendre connaissance des autres agents présents sur la carte
		lb.add(new ExplorerBehaviour(this));
//		lb.add(new SendMapBehaviour(this));
//		lb.add(new ReceiveMapBehaviour(this));
//		lb.add(new SendTreasuresKnowledgeBehaviour(this));
//		lb.add(new ReceiveTreasuresKnowledgeBehaviour(this));
		
		addBehaviour(new startMyBehaviours(this,lb));
		
		System.out.println("the  agent "+this.getLocalName()+ " is started");

	}

	public String getService() {
		return "exploring";
	}
	
}
