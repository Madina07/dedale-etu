package eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.ours.DedaleAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class Ping extends OneShotBehaviour {
	
	private int exitValue;
	
	public Ping(final Agent myAgent) {
		super(myAgent);
	}
	
	@Override
	public void action() {
		System.out.println(this.myAgent.getLocalName() + " PING");
		// envoi ping
		ACLMessage msg=new ACLMessage(ACLMessage.REQUEST);
		msg.setSender(this.myAgent.getAID());
		msg.setProtocol("Anyone here ?");
		for (String lc : ((DedaleAgent)this.myAgent).getAllAgentsOnMap()) {
			msg.addReceiver(new AID(lc,AID.ISLOCALNAME));
		}

		((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
		
		// l'agent regarde s'il a recu un ping, si oui il termine avec la valeur 2 pour entamer un échange d'informations
		final MessageTemplate requestTemplate = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
		final ACLMessage request = this.myAgent.receive(requestTemplate);
		
		if (request != null) {
			if (request.getProtocol() == "Anyone here ?") {
				exitValue = 2;
			}
		} else {
			exitValue = 7;
		}
	}
	
	@Override
	public int onEnd(){
		System.out.println(this.myAgent.getLocalName() + " FIN PING " + exitValue);
		return exitValue;
	}

}
