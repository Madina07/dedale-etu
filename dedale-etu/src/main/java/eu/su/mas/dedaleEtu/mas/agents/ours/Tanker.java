package eu.su.mas.dedaleEtu.mas.agents.ours;

import java.util.ArrayList;
import java.util.List;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.ReceiveTreasureTankerBehaviour;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.df.DFRegisteringBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.df.GetOtherAgentsLocalNamesBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.ReceiveMapBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange.SendMapBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.explo.MapDiscoveryBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ours.silo.TankerBehaviour;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;


/**
 * Dummy Tanker agent. It does nothing more than printing what it observes every 10s and receiving the treasures from other agents. 
 * <br/>
 * Note that this last behaviour is hidden, every tanker agent automatically possess it.
 * 
 * @author hc
 *
 */
public class Tanker extends DedaleAgent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1784844593772918359L;



	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	protected void setup(){

		super.setup();

		List<Behaviour> lb=new ArrayList<Behaviour>();
		
		this.setService("silo");
		
		lb.add(new DFRegisteringBehaviour(this)); // s'enregistrer auprès du DF
		lb.add(new GetOtherAgentsLocalNamesBehaviour(this)); // prendre connaissance des autres agents présents sur la carte
		lb.add(new TankerBehaviour(this));
//		lb.add(new MapDiscoveryBehaviour(this)); // explorer la carte
//		lb.add(new SendMapBehaviour(this));
//		lb.add(new ReceiveMapBehaviour(this));
//		lb.add(new RandomTankerBehaviour(this));
//		lb.add(new ReceiveTreasureTankerBehaviour(this, this.getBackPack()));
		
		addBehaviour(new startMyBehaviours(this,lb));
		
		System.out.println("the  agent "+this.getLocalName()+ " is started");

	}
	
	public String getService() {
		return "silo";
	}

}


/**************************************
 * 
 * 
 * 				BEHAVIOUR
 * 
 * 
 **************************************/

class RandomTankerBehaviour extends TickerBehaviour{
	/**
	 * When an agent choose to migrate all its components should be serializable
	 *  
	 */
	private static final long serialVersionUID = 9088209402507795289L;

	public RandomTankerBehaviour (final AbstractDedaleAgent myagent) {
		super(myagent, 10000);
	}

	@Override
	public void onTick() {
		//Example to retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();

		if (myPosition!=""){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			System.out.println(this.myAgent.getLocalName()+" -- list of observables: "+lobs);
		
		}

	}

}