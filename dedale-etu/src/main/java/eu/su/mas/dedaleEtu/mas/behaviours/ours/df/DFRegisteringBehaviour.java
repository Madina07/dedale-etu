package eu.su.mas.dedaleEtu.mas.behaviours.ours.df;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.ours.DedaleAgent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class DFRegisteringBehaviour extends OneShotBehaviour {

	/**
	 * Un agent s'enregistre en tant que collecteur auprès du DFF
	 */
	private static final long serialVersionUID = 1L;
	
	public DFRegisteringBehaviour(AbstractDedaleAgent myAgent) {
		super(myAgent);
	}
	
	@Override
	public void action() {
		
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(super.myAgent.getAID());
		ServiceDescription sd  = new ServiceDescription();
		sd.setType(((DedaleAgent) this.myAgent).getService()); /* service propose par l'agent*/
		sd.setName(super.myAgent.getLocalName() );
		dfd.addServices(sd);
		try {
			DFService.register(super.myAgent, dfd );
		} catch (FIPAException fe) {fe.printStackTrace();}
	}
}

