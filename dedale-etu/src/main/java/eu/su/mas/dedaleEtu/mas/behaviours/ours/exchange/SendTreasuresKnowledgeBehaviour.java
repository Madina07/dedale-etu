package eu.su.mas.dedaleEtu.mas.behaviours.ours.exchange;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.ours.DedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class SendTreasuresKnowledgeBehaviour extends OneShotBehaviour {
	

	private static final long serialVersionUID = 1L;
	private HashMap<String,HashMap<String,String>> previousSenttk = new HashMap<String,HashMap<String,String>>();
	private HashMap<String,HashMap<String,String>> tkToSend = new HashMap<String,HashMap<String,String>>();
	private int sametkCounter = 0;
	private int noRequest = 0;
	private boolean finished = false;
	
	public SendTreasuresKnowledgeBehaviour(final Agent myAgent) {
		super(myAgent);
	}

	@Override
	public void action() {
		System.out.println(this.myAgent.getLocalName() + " SEND TK BEHAVIOUR");
//		final MessageTemplate requestTemplate = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
//		final ACLMessage request = this.myAgent.receive(requestTemplate);
//		if (request != null) {
//			if (request.getProtocol() == "Send treasures knowledge please") {
				String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
				ACLMessage tk = new ACLMessage(ACLMessage.INFORM);
				tk.setSender(this.myAgent.getAID());
				tk.setProtocol("Send treasures knowledge");
				//if (myPosition!="" && !((DedaleAgent)this.myAgent).getAllAgentsOntk().isEmpty()){
				if (myPosition!=""){
					//System.out.println(this.myAgent.getLocalName()+ " is trying to send his tk");
					try {
						if(((DedaleAgent)this.myAgent).getTreasuresKnowledge()!=null && !((DedaleAgent)this.myAgent).getTreasuresKnowledge().isEmpty()) {
							tkToSend = ((DedaleAgent) this.myAgent).getTreasuresKnowledge();
							tk.setContentObject(tkToSend);
							//System.out.println(this.myAgent.getLocalName()+ " edges to send : " + edgesToSend);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					for (String aln : ((DedaleAgent) this.myAgent).getAllAgentsOnMap())
						tk.addReceiver(new AID(aln,AID.ISLOCALNAME));
//					if (previousSenttk.equals(tkToSend)) {
//						sametkCounter += 1;
//					} else {
//						sametkCounter = 0;
//					}
//					if (sametkCounter == 50) {
//						System.out.println(this.myAgent.getLocalName()+ " already sent this same tk many times");
//						finished = true;
//					}
					System.out.println(this.myAgent.getLocalName()+ " sends his tk : " + tkToSend);
					((AbstractDedaleAgent)this.myAgent).sendMessage(tk);
				}
				previousSenttk = tkToSend;
				noRequest = 0;
			//} 
		//}
//		else {
//			noRequest += 1;
//			if (noRequest == 10000) {
//				System.out.println(this.myAgent.getLocalName()+ " no one wants to know about the treasures I know...");
//				finished = true;
//			}
//		}
	}
	
	@Override
	public int onEnd(){
		System.out.println("FIN SEND TK BEHAVIOUR");
		return 4;
	}

//	@Override
//	public boolean done() {
//		if (finished) {
//			System.out.println(this.myAgent.getLocalName()+" : PING AND SEND TREASURES KNOWLEDGE BEHAVIOUR REMOVED");
//		}
//		return finished;
//	}

}
