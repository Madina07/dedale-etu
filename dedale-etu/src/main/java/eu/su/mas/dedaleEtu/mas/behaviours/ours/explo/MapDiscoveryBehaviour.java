package eu.su.mas.dedaleEtu.mas.behaviours.ours.explo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.graphstream.graph.Node;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.ours.DedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.behaviours.OneShotBehaviour;

public class MapDiscoveryBehaviour extends OneShotBehaviour {
	

	private static final long serialVersionUID = 1L;

	private boolean finished = false;

	/**
	 * Nodes known but not yet visited
	 */
	private List<String> openNodes;
	/**
	 * Visited nodes
	 */
	private Set<String> closedNodes;
	
	/**
	 * Nodes known but not yet visited
	 */
	private Set<String> openTreasures;
	/**
	 * Visited nodes
	 */
	private Set<String> closedTreasures;
	
	private int stuckCounter = 0;
	
	private String previousNextNode;
	
	private String myPreviousPosition; 
	
	private HashMap<String,HashMap<String,String>> treasuresKnowledge = new HashMap<String, HashMap<String, String>>();
	
	private int exitValue = 1;
	
	/**
	 * Permet de retenir la date de la dernière maj des connaissances pour chaque trésor
	 */
	private Date d = new Date();
	
	Random r;

	public MapDiscoveryBehaviour(final AbstractDedaleAgent myAgent) {
		super(myAgent);
		this.openNodes=new ArrayList<String>();
		this.closedNodes=new HashSet<String>();
		this.openTreasures=new HashSet<String>();
		this.closedTreasures=new HashSet<String>();
		this.r= new Random();
	}

	@Override
	public void action() {
		System.out.println(this.myAgent.getLocalName() + " MAP DISCOVERY");
		if (((DedaleAgent)this.myAgent).getMap() == null) {
			((DedaleAgent)this.myAgent).setMap(new MapRepresentation());
		}
		
		//0) Retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
	
		if (myPosition!=null){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			//System.out.println(this.myAgent.getLocalName()+" "+lobs);
					
			/**
			 * Just added here to let you see what the agent is doing, otherwise he will be too quick
			 */
			try {
				this.myAgent.doWait(500);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//1)remove the current node from openlist and add it to closedNodes.
			this.closedNodes.add(myPosition);
			this.openNodes.remove(myPosition);

			//((DedaleAgent)this.myAgent).getMap().addNode(myPosition,MapAttribute.closed);
			((DedaleAgent)this.myAgent).getMap().addNode(myPosition,MapAttribute.me);
			
			//3) Maj closed/opened nodes
			
			for (Node n : ((DedaleAgent)this.myAgent).getMap().getG().getNodeSet()) {
				//System.out.println(this.myAgent.getLocalName() + " : " + n.getId() + " " + n.getAttribute("ui.class"));
				if (n.getAttribute("ui.class") == MapAttribute.open.toString()) {
					if (!this.openNodes.contains(n.getId()) && !this.closedNodes.contains(n.getId())) {
						//System.out.println(this.myAgent.getLocalName() + " : adding node " + n.getId() + " to closedNodes");
						this.openNodes.add(n.getId());
					}
				} else {
					this.closedNodes.add(n.getId());
					if (this.openNodes.contains(n.getId())) {
						//System.out.println(this.myAgent.getLocalName() + " : removing node " + n.getId() + " from openNodes");
						this.openNodes.remove(n.getId());
					}
				}
			}

			//2) get the surrounding nodes and, if not in closedNodes, add them to open nodes.
			String nextNode=null;
			Iterator<Couple<String, List<Couple<Observation, Integer>>>> iter=lobs.iterator();
			while(iter.hasNext()){
				String nodeId=iter.next().getLeft();
				if (!this.closedNodes.contains(nodeId)){
					if (!this.openNodes.contains(nodeId)){
						this.openNodes.add(nodeId);
						((DedaleAgent)this.myAgent).getMap().addNode(nodeId, MapAttribute.open);
						((DedaleAgent)this.myAgent).getMap().addEdge(myPosition, nodeId);	
					}else{
						//the node exist, but not necessarily the edge
						((DedaleAgent)this.myAgent).getMap().addEdge(myPosition, nodeId);
					}
					if (nextNode==null) nextNode=nodeId;
				}
			}
			
			//((DedaleAgent)this.myAgent).getMap().addNode(myPosition,MapAttribute.me);
			
			//System.out.println(this.myAgent.getLocalName() + " : mes noeuds ouverts " + this.openNodes);
			//System.out.println(this.myAgent.getLocalName() + " : mes noeuds fermes " + this.closedNodes);

			//4) while openNodes is not empty, continues.
			if (this.openNodes.isEmpty()){
				//Explo finished
				finished=true;
				exitValue =  8;
				System.out.println(this.myAgent.getLocalName()+" : EXPLORATION SUCCESSFULLY DONE, BEHAVIOUR REMOVED.");
			}else{
				//4) select next move.
				//4.1 If there exist one open node directly reachable, go for it,
				//	 otherwise choose one from the openNode list, compute the shortestPath and go for it
				
				if (nextNode==null){
					//no directly accessible openNode
					//chose one, compute the path and take the first step.
					nextNode=((DedaleAgent)this.myAgent).getMap().getShortestPath(myPosition, this.openNodes.get(0)).get(0);
				}
				
				//System.out.println(this.myAgent.getLocalName() + " is on node " + myPosition + " and wants to go on node " + nextNode);
				
				// Gestion des interblocages : Si l'agent tente d'aller sur un noeud mais n'y parvient pas c'est qu'il est en situation d'interblocage
				// S'il reste bloqué pendant 3 tours alors on le force à choisir un autre noeud sur lequel aller
				// Plutot faire un FSM exploration normale / situation d'interblocage (échanger des messages pour trouver un moyen de débloquer le passage)
				
				// Attention passer a un autre behaviour lorsque on finit celui-là sinon on ne gère plus les interblocages
				
//				if (myPosition == myPreviousPosition && previousNextNode == nextNode) { // si l'agent n'a pas bougé
//					this.closedNodes.add(nextNode);
//					this.openNodes.remove(nextNode);
//					stuckCounter += 1;
//				} else {
//					stuckCounter = 0;
//				}
//				//System.out.println(stuckCounter);
//				if (stuckCounter == 5) {
//					System.out.println("Interblocage détecté");
//					int cpt = 0;
//					if (lobs.size() > 2) {
//						String potentialNextNode = lobs.get((Integer)r.nextInt(lobs.size())).getLeft();
//						//System.out.println("Potential next node :"+potentialNextNode);
//						while (cpt < 5 && (potentialNextNode == myPosition || potentialNextNode == nextNode)) {
//							potentialNextNode=lobs.get((Integer)r.nextInt(lobs.size())).getLeft();
//							cpt += 1;
//							//System.out.println("Next node :"+nextNode);
//						}
//						nextNode = potentialNextNode;
//						//System.out.println("Agent " + this.myAgent.getLocalName() + " is on node " + myPosition + " and wants to go on node " + nextNode);
//					}
//					stuckCounter = 0;
//				}
//				
//				previousNextNode = nextNode;
//				myPreviousPosition = myPosition;
				
				/***************************************************
				** 		ADDING the API CALL to illustrate their use **
				*****************************************************/

				//list of observations associated to the currentPosition
				List<Couple<Observation,Integer>> lObservations= lobs.get(0).getRight();

				//example related to the use of the backpack for the treasure hunt
//				Boolean a = ((DedaleAgent) this.myAgent).getService().equals("collecting");
//				Boolean b = false;
//				if (a) {
//					for(Couple<Observation,Integer> o:lObservations){
//						switch (o.getLeft()) {
//						case DIAMOND:case GOLD:
//							
//							System.out.println(this.myAgent.getLocalName()+" - My treasure type is : "+((AbstractDedaleAgent) this.myAgent).getMyTreasureType());
//							System.out.println(this.myAgent.getLocalName()+" - My current backpack capacity is:"+ ((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace());
//							System.out.println(this.myAgent.getLocalName()+" - My expertise is: "+((AbstractDedaleAgent) this.myAgent).getMyExpertise());
//							//System.out.println(this.myAgent.getLocalName()+" - I try to open the safe: "+((AbstractDedaleAgent) this.myAgent).openLock(Observation.GOLD));
//							System.out.println(this.myAgent.getLocalName()+" - Value of the treasure on the current position: "+o.getLeft() +": "+ o.getRight());
//							if (a) System.out.println(this.myAgent.getLocalName()+" - The agent grabbed : "+((AbstractDedaleAgent) this.myAgent).pick());
//							if (((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace() == 0) {
//								System.out.println(this.myAgent.getLocalName()+" - My backpack is full");
//							} else {
//								System.out.println(this.myAgent.getLocalName()+" - The remaining backpack capacity is: "+ ((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace());
//							}
//							b=true;
//							break;
//						default:
//							break;
//						}
//					}
//				}
				
				Boolean b=false;
				//example related to the use of the backpack for the treasure hunt
				for (int i = 0; i < lobs.size(); i++) {
					String nodeId = lobs.get(i).getLeft();
					List<Couple<Observation,Integer>> obs = lobs.get(i).getRight();
					HashMap<String,String> characteristics = new HashMap<String,String>();
					if (treasuresKnowledge.get(nodeId) != null) {
						characteristics = treasuresKnowledge.get(nodeId);
					}
					//System.out.println(this.myAgent.getLocalName()+" - obs : " + obs);
					List<Couple<Observation,Integer>> obs2 = new ArrayList<>();
					for (int j = 0; j < obs.size(); j++) {
						obs2.add(new Couple(null,0));
					}
					for(Couple<Observation,Integer> o2:obs2){
						for(Couple<Observation,Integer> o:obs){
							switch (o.getLeft()) {
							case DIAMOND:case GOLD:
								obs2.set(0, o);
								break;
							case LOCKSTATUS:
								obs2.set(1, o);
								break;
							case STRENGH:
								obs2.set(2, o);
								break;
							case LOCKPICKING:
								obs2.set(3, o);
								break;
							default:
								break;
							}
						}
					}
					obs = obs2;
					//System.out.println(this.myAgent.getLocalName()+" - obs2 : " + obs);
					openTreasures = ((DedaleAgent)this.myAgent).getOpenTreasures();
					closedTreasures = ((DedaleAgent)this.myAgent).getClosedTreasures();
					boolean success = false;
					for(Couple<Observation,Integer> o:obs){
						switch (o.getLeft()) {
						case DIAMOND:case GOLD:
							//System.out.println(this.myAgent.getLocalName()+" - Value of the treasure on the current position: "+o.getLeft() +": "+ o.getRight());
							if (((DedaleAgent) this.myAgent).getService().equals("exploring")) success = ((AbstractDedaleAgent) this.myAgent).openLock(o.getLeft());
							//System.out.println(this.myAgent.getLocalName()+" - I try to open the safe: "+success);
							if (!openTreasures.contains(nodeId) && success) {
								System.out.println(this.myAgent.getLocalName()+" - I opened the safe on node " + nodeId); 
								//maj
								//obs = ((AbstractDedaleAgent)this.myAgent).observe().get(i).getRight();
								System.out.println(this.myAgent.getLocalName()+" - new obs : " + obs);
							} else {
								System.out.println(this.myAgent.getLocalName()+" - I couldn't open the safe on node " + nodeId);
							}
							characteristics.put("type", o.getLeft().toString());
							characteristics.put("value", o.getRight().toString());
							b=true;
							break;
						case LOCKSTATUS:
							if (o.getRight() == 1 || success){
								if (!openTreasures.contains(nodeId)) {
									//openTreasures.add(nodeId);
									characteristics.put("isOpen", "1");
								}
							} else {
								//closedTreasures.add(nodeId);
								characteristics.put("isOpen", "0");
							}
							break;
						case STRENGH:
							characteristics.put("strengthRequired", o.getRight().toString());
							break;
						case LOCKPICKING:
							characteristics.put("lockpickingRequired", o.getRight().toString());
							break;
						default:
							break;
						}
					}
					if (!characteristics.isEmpty()) {
						characteristics.put("lastMaj",Long.toString(d.getTime()));
						treasuresKnowledge.put(nodeId, characteristics);
						((DedaleAgent)this.myAgent).setTreasuresKnowledge(treasuresKnowledge);
						System.out.println(this.myAgent.getLocalName()+" - characteristics : " + characteristics);
					}
				}
					
				//If the agent picked (part of) the treasure
//				if (a && b){
//					List<Couple<String,List<Couple<Observation,Integer>>>> lobs2=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
//					System.out.println(this.myAgent.getLocalName()+" - State of the observations after picking "+lobs2);
//				}
					
				ArrayList<String> tankersLocalNames =  new ArrayList<String>();
				if (((DedaleAgent) this.myAgent).getTankersOnMap() != null) tankersLocalNames = ((DedaleAgent) this.myAgent).getTankersOnMap();
					
				//Trying to store everything in the tanker
				if (!tankersLocalNames.isEmpty()) {
					System.out.println(this.myAgent.getLocalName()+" - My current backpack capacity is:"+ ((AbstractDedaleAgent)this.myAgent).getBackPackFreeSpace());
					for (String tanker : tankersLocalNames) {
						System.out.println(this.myAgent.getLocalName()+" - The agent tries to transfer is load into the Silo (if reachable); succes ? : "+((AbstractDedaleAgent)this.myAgent).emptyMyBackPack(tanker));
					}
					System.out.println(this.myAgent.getLocalName()+" - My current backpack capacity is:"+ ((AbstractDedaleAgent)this.myAgent).getBackPackFreeSpace());
					
				}
				System.out.println(this.myAgent.getLocalName()+" - openNodes : " + openNodes);
				System.out.println(this.myAgent.getLocalName()+" - closedNodes : " + closedNodes);
				System.out.println(this.myAgent.getLocalName()+" - openTreasures : " + openTreasures);
				System.out.println(this.myAgent.getLocalName()+" - closedTreasures : " + closedTreasures);
				exitValue = 1;
				/************************************************
				 * 				END API CALL ILUSTRATION
				 *************************************************/
				System.out.println(this.myAgent.getLocalName() + " is on node " + myPosition + " and moves to node " + nextNode);
				((AbstractDedaleAgent)this.myAgent).moveTo(nextNode);
				//System.out.println(this.myAgent.getLocalName() + " moves to node " + nextNode);
			}

		}
	}

	public int onEnd(){
		System.out.println(this.myAgent.getLocalName() + " FIN MAP DISCOVERY " + exitValue);
		return exitValue;
	}
	
//	@Override
//	public boolean done() {
//		return finished;
//	}

}
